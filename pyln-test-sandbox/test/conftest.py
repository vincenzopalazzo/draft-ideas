import pytest
from src import Bitcoind


@pytest.fixture(scope="session")
def bitcoind():
    rpc = Bitcoind("/tmp/pyln-test")
    return rpc

@pytest.fixture(scope="session", autouse=True)
def manage_runner(bitcoind):
    bitcoind.start()
    yield
    bitcoind.stop()
