/**
 * draft-ideas
 *
 * Copyright (C) 2021  Vincenzo Palazzo vincenzopalazzodev@gmail.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package io.github.vincenzopalazzo.tooltip;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MapPin extends JLabel {
    public MapPin(String namePosition) {
        super();
        this.setToolTipText(namePosition);
        this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        //this.setText(namePosition);
        String Path = this.getClass().getClassLoader().getResource("placeholder-filled-point.png").getPath();
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(Path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setIcon(new ImageIcon(image));
    }
}
